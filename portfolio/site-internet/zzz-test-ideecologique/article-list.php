<?php
require_once 'tools/common.php';
?>

<?php
if(isset($_GET['category_id'])) {

    $query = $db->prepare('SELECT * FROM category WHERE id = ?');
    $query->execute(array($_GET['category_id']));
    $donnees = $query->fetch();

    if ($donnees) {

        $query = $db->prepare('SELECT articles_projects.* FROM articles_projects JOIN projects_category ON projects_category.project_id  = articles_projects.id JOIN category ON category.id = projects_category.category_id
                WHERE category.id = ? GROUP BY articles_projects.id');
        $query->execute(array($_GET['category_id']));
        $result = $query->fetchAll();
    } else { //si la catégorie n'existe pas, redirection vers la page d'accueil
        header('location:index.php');
        exit;
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require 'partials/head_assets.php' ?>
        <link rel="stylesheet" href="style-projects-locaux-internationaux.css">
        <title>projets locaux et internationaux</title>
    </head>
    <body>

        <?php require 'partials/header.php' ?>
        <div class="container-fluid photo-main"></div>
        <?php require 'partials/nav.php' ?>

        <div class="container nav2">
            <p><a href="" class="link-accueil">Accueil</a> > Projets locaux et internationaux ></p>
            <hr class="hr-nav2">
        </div>



        <div class="container">
            <div class="row">
                <?php if(!empty($result)):?>
                    <?php foreach($result AS $donnee): ?>
                        <div class="col-xl-6">
                            <img class="card-img-top" src="<?php echo $donnee['file_url']; ?>" alt="Card image cap" >
                        </div>
                        <div class="col-xl-6">
                            <p class="title-article"><strong><?php echo $donnee['title']; ?></strong></p>
                            <p class="sous-title-article"><?php echo $donnee['content']; ?></p>
                            <a class="btn btn-primary" href="<?php echo $donnee['href']; ?>" role="button">Lire <i class="fas fa-arrow-right"></i></a>
                        </div>
                    <?php endforeach; ?>
                <?php else:?>
                    <p>Aucun article est disponible pour ce article.</p>
                <?php endif; ?>
            </div>
        </div>
    </body>
</html>
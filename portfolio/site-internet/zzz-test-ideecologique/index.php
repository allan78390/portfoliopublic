<!DOCTYPE html>
    <html>
    <head>
        <?php require 'partials/head_assets.php' ?>
        <link rel="stylesheet" href="style.css">
        <title>Homepage-Mon site à moi !!</title>
    </head>
    <body>

    <?php require 'partials/header.php' ?>

    <div class="container-fluid photo-main">
        <div class="row">
            <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-md-6 offset-md-2 col-sm-10 offset-sm-2 col-7  title">
                <p class="nom-site">IDEECOLOGIQUE</p>
                <p class="sous-titre">Il n'y a pas de petite idées</p>
                <a href="file:///Users/allan/Desktop/projet-ideecologique/idee.html" class="btn" role="button">POST TON IDEE</a>
            </div>
        </div>
    </div>

    <?php require 'partials/nav.php' ?>

    <div class="container nav2">
        <p><a href="" class="link-accueil">Accueil</a> > About</p>
        <hr class="hr-nav2">
    </div>
    <div class="container">
        <div class="row presentation">
            <p>J'ai lancé le blog ideecologique en 2018, depuis je partage avec vous mes points de vue écologique, mes astuces, mes trouvailles et surtout mes idées.</p>
            <p>Je pense que le plus important c'est que chacun apporte sa pierre à l'édifice, et surtout protéger notre précieuse planéte ou après tout nous ne faisons que passer.</p>
            <p>J'y aborde des sujets variés qui répondent à des besoins fondamentaux que rencontre les hommes tous les jours sur terre.Ils suivent une ligne de conduite reposant sur quatre piliers principaux que sont la nourriture, l'eau,l'énergie et l'habitat qui sont des problèmes que chacun peut solutionner par son vécu et son expérience.</p>
            <p>Ainsi, je vous posterai une nouvelle idée et/ou projet chaque semaine afin de vous tenir informer des dérnière actualités dans le monde</p>
            <p>Trés bonne visite sur ideecologique et n'hésitez pas à poster vos idées mais aussi partager ou collaborer sur la conception de chacune d'entre elles dans la page open source !!</p>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container">
        <div class="row statistique">
            <div class="col-xl-5 offset-xl-4 col-lg-5 offset-lg-3 col-md-8 offset-md-2 col-sm-10 offset-sm-2 col-10 offset-2">
                Quelques statistiques
            </div>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-2 offset-xl-1 col-lg-2 offset-lg-1 col-md-2 offset-md-1 col-sm-4 col-4">
                <i class="fas fa-shopping-cart fa-2x color-nourriture"></i>
                <p>Nourriture</p>
                <p>Il faudrait augmenter de 40% la surface<br/> des terres cultivables pour nourrir la<br/> planéte dans 40 ans</p>
            </div>
            <div class="col-xl-2 offset-xl-2 col-lg-2 offset-lg-2 col-md-2 offset-md-2 col-sm-4 col-4">
                <i class="fas fa-tint fa-2x color-water"></i>
                <p>Eau</p>
                <p>Selon  les estimations des scientifiques,<br/>dans 50 ans, les ressources en eau<br/>potable vont diminuer d'un tiers ou<br/> de moitier dans le monde.</p>
            </div>
            <div class="col-xl-2 offset-xl-2 col-lg-2 offset-lg-2 col-md-2 offset-md-2 col-sm-4 col-4">
                <i class="fas fa-battery-half fa-2x color-energie"></i>
                <p>Energie</p>
                <p>La fin des énergies fossiles attendue dans moins de... 50 ans</p>
            </div>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 offset-xl-5 col-lg-3 offset-lg-5 col-md-3 offset-md-5 col-sm-4 offset-sm-4 col-sm-4 offset-sm-4 col-4 offset-4">
                <i class="fas fa-home fa-2x"></i>
                <p>Habitat</p>
                <p>En Ile de France, un logement = 18 <br/>années de revenus</p>
            </div>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-10 offset-xl-2 col-lg-10 offset-lg-2 col-md-11 offset-md-1">
                <p class="statistique">Quelques exemples de projets de niveau international.</p>
                <p>Et ce n'est que le début...</p>
            </div>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <img src="image/ferme-urbaine.jpeg" class="taille-ferme-urbaine">
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6  description">
                <p><strong>Ferme urbaine</strong></p>
                <p>SINGAPOUR</p>
                <p>Des capteurs mesurent en temps<br/> réel le taux d'humidité, la luminosité et la qualité de l'air pour obtenir les paramétres parfaits à la croissance de la plante.</p>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <img src="image/dessalinisateur%20d'eau%20de%20mer.jpg" class="taille-dessalinasteur">
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 description">
                <p><strong>Dessalinisateur d'eau de mer</strong></p>
                <p>ABU DHABI</p>
                <p>Solution photovoltaique d'eau de dessalement d'eau de mer.Peu gourmand en énergie, ce systéme est capable de s'adapter à l'ensoleillementet fonctionne sans batterie.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <img src="image/tour%20solaire.jpeg" class="taille-tour-solaire">
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 description-ferme-urbaine">
                <p><strong>Tour solaire</strong></p>
                <p>ISRAEL</p>
                <p>Une tour solaire est une central à énergie renouvelable, construite de manière à canalisé l'air chauffé par le soleil afin d'actionner des turbines pour produire de l'éléctrecité.</p>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                <img src="image/imprimante-3D.jpg" class="taille-tour-solaire">
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 description-ferme-urbaine">
                <p><strong>Imprimante 3D</strong></p>
                <p>CHINE</p>
                <p>Une sociéte chinoise à << imprimé >> dix maison en 24h grace à une gigantesque imprimante 3D/p>
            </div>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-xl-9 offset-xl-2 col-lg-10 offset-lg-2 col-md-10 offset-md-1">
                <p class="statistique">Quelques exemples de projets de niveau local</p>
                <p>L'Imagination est sans limite</p>
            </div>
        </div>
    </div>
    <div class="espace1"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-4  col-lg-4  col-md-4">
                <img src="image/shower-loop.jpg" class="image-showerloop">
                <p class="big-title1"><strong>Une douche qui recycle de l'eau en temps réel: Showerloop</strong></p>
                <p class="big-under-title2" >En ajoutant un filtre raccordé au tuyaux d'eau.Grace à cette installation déroutante de simplicité, la consomation d'eau peut-etre reduite de 90% pendant la douche.</p>
                <p class="phrase-premier-mot">Quand chaque probléme </p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4">
                <img src="image/wind-turbine-poc21.jpg" class="image-showerloop">
                <p class="big-title2"><strong>Un vent de nouveauté: Wind Turbine</strong></p>
                <p class="big-under-title">L'objet commence à engrangé de l'énergie des 5KM/H de vent, l'éolienne miniature la convertit la convertit en 6KW d'éléctrécité.</p>
                <p class="phrase-deuxieme-mot"> devient</p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4">
                <img src="image/mineral-water.jpg" class="image-showerloop">
                <p class="big-title3"><strong>De l'eau potable pour tous: Faircap</strong></p>
                <p class="big-under-title">Développement d'un filtre à eau miniature, portable, peu cher et que l'on fixersur n'importe qu'elle bouteille.</p>
                <p class="phrase-troisieme-mot">une opportunités..</p>
            </div>
        </div>
    </div>

    <?php require 'partials/footer.php' ?>

    </body>
</html>



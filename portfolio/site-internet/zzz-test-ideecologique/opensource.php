<?php
require 'tools/common.php';
$query = $db->query('SELECT * FROM inventions');
$donneess = $query->fetchAll();
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require 'partials/head_assets.php' ?>
        <link rel="stylesheet" href="styleopen-source.css">
        <title>Projets Open-Source</title>
    </head>
    <body>

        <?php require 'partials/header.php' ?>
        <div class="container-fluid photo-main"></div>
        <?php require 'partials/nav.php' ?>

        <div class="container nav2">
            <p><a href="" class="link-accueil">Accueil</a> > Open source</p>
            <hr class="hr-nav2">
        </div>
        <div class="container">
            <div class="row">
                <p class="sous-titre">Si pour toi aussi c'est important d'en faire profiter tout le monde!!</p>
                <p class="sous-titre">Parce que un produit est toujours en évolution collabore sur/ou améliore sur un projet qui t'interresse!!</p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <?php if($donneess):?>
                    <?php foreach($donneess AS $donnee): ?>
                        <div class="col-xl-6">
                            <img class="card-img-top" src="<?php echo $donnee['file_url']; ?>" alt="Card image cap" >
                        </div>
                        <div class="col-xl-6">
                            <p class="title-article"><strong><?php echo $donnee['title']; ?></strong></p>
                            <p class="sous-title-article"><?php echo $donnee['content']; ?></p>
                            <a class="btn btn-primary" href="<?php echo $donnee['href']; ?>" role="button">Lire <i class="fas fa-arrow-right"></i></a>
                        </div>
                    <?php endforeach; ?>
                <?php else:?>
                    <p>Aucune image est disponible pour ce article.</p>
                <?php endif; ?>
            </div>
        </div>

    </body>
</html>
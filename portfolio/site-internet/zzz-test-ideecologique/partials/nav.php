
<?php
require 'tools/common.php';



	$query = $db->query('SELECT * FROM category');

?>


<div class="container-fluid theone">
    <div class="row no-gutters">
        <div class="col-xl-3  col-lg-3  col-md-3 col-sm-3 col-3">
            <div class="nav-item dropdown">
                <a href="index.php" class="nav-link">
                    <section class="about">About</section>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Idées
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Projets locaux</a>
                    <a class="dropdown-item" href="#">Projets internationaux</a>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="opensource.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Open source
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="opensource.php">Projets locaux</a>
                    <a class="dropdown-item" href="opensource.php">Projets internationaux</a>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Projets locaux et internationaux
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <?php  while($donnees = $query->fetch()): ?>
                    <a class="dropdown-item" href="article-list.php?category_id=<?php echo $donnees['id']; ?>"><?php echo $donnees['title']?></a>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="dropdown-item" href="article-list.php?category_id=<?php echo $donnees['id']; ?>"><?php echo $donnees['name']?></a>


<div class="container-fluid">
    <div class="row one">
        <div class="col-xl-2 col-lg-1 col-md-1 col-sm-1 col-1 last-page" ></div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-1 col-1 last-page">
            <div class="last-page1">
                Les derniéres nouvelles
            </div>
        </div>
        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-2 col-2 text-white"><span class="badge badge-danger">Exclusif</span></div>
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4 text-white">
            <?php
            $heure = date('h:i');
            ?>
            <marquee behavior="scroll" class="defilement">Dernier article publié il y a  <?php echo $heure; ?> min sur une éolienne qui produit de l'eau potable.</marquee>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-4 dayone">
            <div class="day">
                <?php $var = date('d/m/y');
                echo $var;
                ?>
            </div>
        </div>
    </div>
</div>

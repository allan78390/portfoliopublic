<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <script src="https://use.fontawesome.com/720a5d4092.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Work+Sans" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="style.css">

</head>
<body>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                <img src="image/nouveaux%20s.jpg" alt="photo un s" class="photo-nav" >
            </div>
            <div class="col-xl-1 offset-xl-10 col-lg-1 offset-lg-10  col-md-1 offset-md-10 col-sm-1 offset-sm-10 col-1 offset-10">
                <div class="dropdown">
                    <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdownContact">
                        <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="home.php">home</a>
                        <a class="dropdown-item" href="contemporain.php">contemporary</a>
                        <a class="dropdown-item" href="modern.php">modern</a>
                        <a class="dropdown-item" href="histoire.php">history</a>
                        <a class="dropdown-item" href="objectif.php">objectifs</a>
                        <a class="dropdown-item" href="buy.php">buy</a>
                        <a class="dropdown-item" href="contacts.php">contacts</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid animated fadeIn">
        <div class="row no-gutters">
            <div class="col-xl-4 offset-xl-4">
            <h1 class="header-title"> Snow-Contact </h1>
        </div>
            <div class="container-fluid">
                <div class="col-xl-12" id="parent">
                    <div class="col-xl-6">
                        <iframe width="100%" height="320px;" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJaY32Qm3KWTkRuOnKfoIVZws&key=AIzaSyAf64FepFyUGZd3WFWhZzisswVx2K37RFY" allowfullscreen></iframe>
                    </div>

                    <div class="col-xl-6">
                        <form action="form.php" class="contact-form" method="post">

                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="nm" placeholder="Name" required="" autofocus="">
                            </div>


                            <div class="form-group form_left">
                                <input type="email" class="form-control" id="email" name="em" placeholder="Email" required="">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" placeholder="Mobile No." required="">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control textarea-contact" rows="5" id="comment" name="FB" placeholder="Type Your Message/Feedback here..." required=""></textarea>
                                <br>
                                <button class="btn btn-default btn-send"> <span class="glyphicon glyphicon-send"></span> Send </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid second-portion">
        <div class="row">
            <div class="col-xl-4">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <h3 class="title">MAIL & WEBSITE</h3>
                            <p>
                                <i class="fa fa-envelope" aria-hidden="true"></i> dubreuilallan@gmail.com
                                <br>
                                <br>
                                <i class="fa fa-globe" aria-hidden="true"></i> dubreuilallan@gmail.com
                            </p>
                        </div>
                    </div>
                    <div class="space"></div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <h3 class="title">CONTACT</h3>
                            <p>
                                <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (07)-9624XXXXX
                                <br>
                                <br>
                                <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp  (07)-7567065254
                            </p>
                        </div>
                    </div>
                    <div class="space"></div>
                </div>
            </div>
            <div class="col-xl-4 ">
                <div class="box">
                    <div class="icon">
                        <div class="info">
                            <h3 class="title">ADDRESS</h3>
                            <p>
                                <i class="fa fa-map-marker" aria-hidden="true"></i> PARIS <br/>60 Quai de Jemmape.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space2"></div>
    <div class="space"></div>
    <div class="container-fluid social-network-index-contact">
        <div class="row">
            <div class="col-xl-3 offset-xl-9 col-lg-3 offset-lg-9 col-md-4 offset-md-8 col-sm-5 offset-sm-7 col-6 offset-6">
                <ul class="social-network social-circle">
                    <li><a href="https://www.facebook.com/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>




</body>
</html>

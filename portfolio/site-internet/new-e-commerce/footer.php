<div class="container espace"></div>

<div class="container-fluid footer1">
    <div class="row">
        <div class="col-xl-6 offset-xl-1">
            <p class="phrase">Piercing ArtStreet : Vente de Piercing et Bijoux fantaisie</p>
        </div>
    </div>
</div>
<div class="container-fluid footer2">
    <div class="row">
        <div class="col-xl-3">
            <div class="rond" class="de"><i class="fas fa-lock fa-2x"></i></div>
            <p class="title-index">Paiement sécurisé</p>
            <p class="sous-title-index">Les moyens de paiement proposés sont tous totalement sécurisés</p>
        </div>
        <div class="col-xl-3">
            <div class="rond"><i class="fas fa-headphones fa-2x"></i></div>
            <p class="title-index">Service client</p>
            <p class="sous-title-index">Le service client est à votre disposition du lundi au vendredi de 9h à 17h30</p>
        </div>

        <div class="col-xl-3">
            <div class="rond"><i class="far fa-clock fa-2x"></i></div>
            <p class="title-index">Expédition sous 24h</p>
            <p class="sous-title-index">Votre commande est préparée et expédiée sous 24h</p>
        </div>
        <div class="col-xl-3">
            <div class="rond"><i class="fas fa-truck fa-2x"></i></div>
            <p class="title-index">Livraison Offerte</p>
            <p class="sous-title-index">La livraison est offert à partir de 20€ d'achats</p>
        </div>


    </div>

</div>
<div class="container-fluid color">
    <div class="row">
        <div class="col-3 offset-1 contact">
            <h2>Informations</h2>
            <hr class="trait-white">
            <p><a href="" class="line-footer">Contactez-nous</a></p>
            <p><a href="" class="line-footer">Questions et réponses</a></p>
            <p><a href="" class="line-footer">Livraisons</a></p>
            <p><a href="" class="line-footer">Paiements sécurisé</a></p>

        </div>
        <div class="col-3 offset-1 contact">
            <h2>Social Networks</h2>
            <hr class="trait-white">
            <a href="https://www.facebook.com/" class="logo"><i class="fab fa-facebook fa-3x"></i></a>
            <a href="https://twitter.com/" class="logo"><i class="fab fa-twitter-square fa-3x"></i></a>
            <a href="https://www.linkedin.com/" class="logo"><i class="fab fa-linkedin fa-3x"></i></a>
            <a href="https://www.instagram.com/?hl=fr" class="logo"><i class="fab fa-instagram fa-3x"></i></a>
        </div>
        <div class="col-3 offset-1 contact">
            <h2>Mon compte</h2>
            <hr class="trait-white">
            <p><a href="" class="line-footer">Mes commandes</a></p>
            <p><a href="" class="line-footer">Mes retours de marchandise</a></p>
            <p><a href="" class="line-footer">Mes avoirs</a></p>
            <p><a href="" class="line-footer">Mes informations personnelles</a></p>

        </div>

    </div>
</div>

</body>
</html>

<?php require '_header.php' ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="animate.css">
    <link href="css/stylee.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="style.css">

</head>
<body>
    <div class="container-fluid home">
        <div class="row">
            <div class="col-2 offset-1">
                <img src="images/allanLOGO.png" class="logo-home">
            </div>
            <div class="col-1 offset-6">

            </div>
            <div class="col-2">
              <img src="images/panier.png" alt="photo de panier" class="photo-panier">
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">

          <ul class="menu cf" style="...">
            <li>
              <a href="">Arcade</a>
              <ul class="submenu">
                <li><a href="">Arcade banane</a></li>
                <li><a href="">Arcade fer à cheval</a></li>
                <li><a href="">Arcade spiral</a></li>
                <li><a href="">Submenu item</a></li>
              </ul>
            </li>
            <li>
              <a href="">Langue</a>
            </li>
            <li>
              <a href="">Lèvre</a>
              <ul class="submenu">
                <li><a href="">Lèvre labret</a></li>
                <li><a href="">Lèvre pointe</a></li>
                <li><a href="">Lèvre boule</a></li>
                <li><a href="">Lèvre loop</a></li>
              </ul>
            </li>
            <li>
              <a href="">Oreille</a>
              <ul class="submenu">
                <li><a href=""></a></li>
                <li><a href="">Piercing cartilage hélix</a></li>
                <li><a href="">Piercing tragus</a></li>

              </ul>
            </li>
            <li>
              <a href="">Nez</a>
              <ul class="submenu">
                <li><a href="">Nez tige droite</a></li>
                <li><a href="">Nez tige courbée</a></li>
                <li><a href="">Nez tige pliable</a></li>
                <li><a href="">Nez tige anneau</a></li>
                  <li><a href="">Nez tige septum</a></li>
              </ul>
            </li>
            <li>
              <a href="">Téton</a>
              <ul class="submenu">
                <li><a href="">Téton anneaux</a></li>
                <li><a href="">Téton barbell</a></li>
                <li><a href="">Téton fer à cheval</a></li>
                <li><a href="">Téton pendentif</a></li>
                  <li><a href="">Téton bouclier</a></li>
                  <li><a href="">Téton homme</a></li>

              </ul>
            </li>
            <li>
              <a href="">Nombril</a>
              <ul class="submenu">
                <li><a href="">Nombril banane</a></li>
                <li><a href="">Nombril pendantif</a></li>
                <li><a href="">Nombril inversé</a></li>
                <li><a href="">Nombril grossesse</a></li>
                  <li><a href="">Nombril anneau</a></li>
                  <li><a href="">Nombril spiral</a></li>
                  <li><a href="">Nombril cristal évolution</a></li>

              </ul>
            </li>
              <li>
                  <a href="">Ecarteur</a>
                  <ul class="submenu">
                      <li><a href="">Piercing plug</a></li>
                      <li><a href="">Piercing écarteur</a></li>
                      <li><a href="">Piercing tunnel</a></li>
                  </ul>
              </li>
              <li>
                  <a href="">Soldes</a>
              </li>
              <li>
                  <a href="">Produits d'entretien</a>
              </li>
          </ul>
        </div>
    </div>

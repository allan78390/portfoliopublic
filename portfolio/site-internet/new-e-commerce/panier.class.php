<?php

   function createPanier(){
     if(!isset($_SESSION['panier'])){
       $_SESSION['panier'] = array();
       $_SESSION['libelleProduit'] = array();
       $_SESSION['prix'] = array();
       $_SESSION['qteProduit'] = array();

     }
      return true;
   }


   function ajouterProduit($libelleProduit, $qteProduit, $prixProduit){
     if(createPanier()){
       $positionProduit = array_search($libelleProduit, $_SESSION['libelleProduit']);
       if($positionProduit){
           $_SESSION['libelleProduit'][$libelleProduit] += $qteProduit;
       }
       else{
           array_push($_SESSION['libelleProduit'], $libelleProduit);
           array_push($_SESSION['qteProduit'], $qteProduit);
           array_push($_SESSION['prix'], $prixProduit);
       }

     }
   }

   function modifierQteProduit($libelleProduit, $qteProduit){
     if(createPanier()){
       $positionProduit = array_search($libelleProduit, $_SESSION['libelleProduit']);
       if($qteProduit > 0){
           $_SESSION['libelleProduit'][$positionProduit] = $qteProduit;
       }
       else{
         supprimerProduit($libelleProduit);
       }

     }
   }

   function compterArticle(){
     if(isset($_SESSION['panier'])){
       return count($_SESSION['panier']['libelleProduit']);
     }

   }


   function supprimerPanier(){
     if(isset($_SESSION['panier'])){
       unset($_SESSION['panier']);
     }
   }

   function montantGlobal(){
     $total = 0;

     for($i = 0; $i < $_SESSION['panier']['libelleProduit']; $i++){
       $total += $_SESSION['panier']['libelleProduit'][$i] * $_SESSION['panier']['prix'];
     }
   }
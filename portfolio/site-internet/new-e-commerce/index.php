<?php require 'header.php'




?>





<div class="container">

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block" src="images/blonde.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block" src="images/blonde-1.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block" src="images/blonde-2.jpg" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block" src="images/blonde-3.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="container-fluid">
    <div class="space"></div>
</div>


    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="#">Populaire</a>
                <a class="nav-item nav-link" href="#">Meilleurs ventes</a>
            </div>
        </div>
    </nav>
        <div class="space2"></div>


    </div>

<div class="container">
    <div class="row">


    <?php $products = $db->query('SELECT * FROM image'); ?>
    <?php foreach($products as $product):  ?>

            <div class="card">
                <img class="card-img-top" src="<?php echo $product['file_url'] ?>" alt="Card image cap">
                <div class="card-body">
                    <p>  <?php  echo number_format($product['price'],2,',',' '); ?>€</p>
                            <h5 class="card-title"><?php  echo $product['name']; ?></h5>

                    <a href="#" class="btn btn-primary">Ajouter au panier</a><br>
                    <a href="produit.php?product_id=<?php echo $product['id']; ?>">Details</a>

                </div>
            </div>
    <?php endforeach; ?>
</div>
</div>

<div class="space2"></div>


<?php require 'footer.php' ?>

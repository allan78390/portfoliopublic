<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <script src="https://use.fontawesome.com/720a5d4092.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="animate.css">

    <link rel="stylesheet" href="style.css">

</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                <img src="image/nouveaux%20s.jpg" alt="photo un s" class="photo-nav" >
            </div>
            <div class="col-xl-1 offset-xl-10 col-lg-1 offset-lg-9 col-md-1 offset-md-10 col-sm-1 offset-sm-10 col-1 offset-9">
                <div class="dropdown">
                    <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="ModernDropdown">
                        <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="home.php">home</a>
                        <a class="dropdown-item" href="contemporain.php">contemporary</a>
                        <a class="dropdown-item" href="modern.php">modern</a>
                        <a class="dropdown-item" href="histoire.php">history</a>
                        <a class="dropdown-item" href="objectif.php">objectifs</a>
                        <a class="dropdown-item" href="buy.php">buy</a>
                        <a class="dropdown-item" href="contacts.php">contacts</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron text-center">
        <h1 class="animated swing" class="skillModern">Skills.Passion.Ideas</h1>
        <p class="animated swing " class="DesignModern" >DESIGN : PHOTOGRAPHY</p>
        <i class="fa fa-th-large" aria-hidden="true"></i>
    </div>
    <div class="container modernContainer">
        <div class="row">
            <div class="col-xl-12 col-12">
                <div class="card-columns">
                    <div class="card">
                        <img class="card-img1-modern" src="image/ananas.jpeg" alt="Card image">
                    </div>
                    <div class="card">
                        <img class="card-img1-modern" src="image/lit2.webp" alt="Card image">
                    </div>
                    <div class="card">
                        <img class="card-img2-modern" src="image/luminaire.jpg" alt="Card image">
                    </div>
                    <div class="card" class="super">
                        <img class="card-img2-modern" src="image/taillere.jpg" alt="Card image cap">
                    </div>
                    <div class="card">
                        <img class="card-img1-modern" src="image/hermes%20sac.jpg" alt="Card image cap">
                    </div>
                    <div class="card">
                        <img class="card-img1-modern" src="image/cocoon.jpg" alt="Card image cap">
                    </div>
                    <div class="card">
                        <img class="card-img1-modern" src="image/table-fleur.jpg" alt="Card image">
                    </div>
                    <div class="card">
                        <img class="card-img2-modern" src="image/chaise%20cuir.jpg" alt="Card image">
                    </div>
                    <div class="card">
                        <img class="card-img1-modern" src="image/lit1.webp" alt="Card image">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="espace-modern"></div>
    <div class="container-fluid social-network-index-modern">
        <div class="row">
            <div class="col-xl-3 offset-xl-9 col-lg-3 offset-lg-9 col-md-4 offset-md-8 col-sm-5 offset-sm-7 col-6 offset-6">
                <ul class="social-network social-circle">
                    <li><a href="https://www.facebook.com/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    </body>
</html>

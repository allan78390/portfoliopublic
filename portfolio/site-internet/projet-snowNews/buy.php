<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <script src="https://use.fontawesome.com/720a5d4092.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Work+Sans" rel="stylesheet">

    <link rel="stylesheet" href="style.css">

</head>
<body>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                <img src="image/nouveaux%20s.jpg" alt="photo un s" class="photo-nav" >
            </div>
            <div class="col-xl-1 offset-xl-10 col-lg-1 offset-lg-10 col-md-1 offset-md-9 col-sm-1 offset-sm-10 col-1 offset-10">
                <div class="dropdown">
                    <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="buy_dropdwn">
                        <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="home.php">home</a>
                        <a class="dropdown-item" href="contemporain.php">contemporary</a>
                        <a class="dropdown-item" href="modern.php">modern</a>
                        <a class="dropdown-item" href="histoire.php">history</a>
                        <a class="dropdown-item" href="objectif.php">objectifs</a>
                        <a class="dropdown-item" href="buy.php">buy</a>
                        <a class="dropdown-item" href="contacts.php">contacts</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-6 col-lg-6 col-md-6 big-titre-buy align-self-center">
                <p class="titre-buy">We create <strong>everything</strong> digital<br/>printable, minimal, <strong>visual<br/> and analytical.</strong></p>
                <p class="fleches"><i class="fa fa-arrow-left" aria-hidden="true"></i>  <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
                <img src="image/photo-ipad.png" alt="phto accesoire" class="photo-accesoire-slide5">
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm12">
                <div class="info-card2">
                    <div class="front2">
                        <img class="card-image2" src="image/table-fleur.jpg">
                    </div>
                    <div class="back2">
                        <h2>Prix: 5400€</h2>
                        <h3> <strong>Description</strong>: Inspiré d'une fleur rare du Népal. </h3>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm12">
                <div class="info-card2">
                    <div class="front2">
                        <img class="card-image2" src="image/lit1.webp">
                    </div>
                    <div class="back2">
                        <h2>Prix: 2400€</h2>
                        <h3> <strong>Description</strong>: Ce joli lit dont le moelleux vous fera tourner la tête!!</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm12">
                <div class="info-card2">
                    <div class="front2">
                        <img class="card-image2" src="image/lit2.webp">
                    </div>
                    <div class="back2">
                        <h2>Prix: 60€</h2>
                        <h3><strong>Description</strong>: Un dégradé de couleurs exeptionnelles ! </h3>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm12">
                <div class="info-card2">
                    <div class="front2">
                        <img class="card-image2" src="image/ananas.jpeg">
                    </div>
                    <div class="back2">
                        <h2>Prix: 60€</h2>
                        <h3><strong>Description</strong>: Un dégradé de couleurs exeptionnelles ! </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm12">
                <div class="info-card2">
                    <div class="front2">
                        <img class="card-image2" src="image/bouteille%20de%20vin.jpeg">
                    </div>
                    <div class="back2">
                        <h2>Prix: 700€</h2>
                        <h3> <strong>Description</strong>: Romano CONTIE 1790 </h3>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm12">
                <div class="info-card2">
                    <div class="front2">
                        <img class="card-image2" src="image/taillere.jpg">
                    </div>
                    <div class=" back2">
                        <h2>Prix: 11750€</h2>
                        <h3> <strong>Description</strong>: Théière de la dynastie Mang </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="space2"></div>
    <div class="container-fluid social-network-index-buy">
        <div class="row">
            <div class="col-xl-3 offset-xl-9 col-lg-3 offset-lg-9 col-md-4 offset-md-8 col-sm-4 offset-sm-7 col-6 offset-6">
                <ul class="social-network social-circle">
                    <li><a href="https://www.facebook.com/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

</body>
</html>

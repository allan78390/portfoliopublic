<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8"/>
            <script src="https://use.fontawesome.com/720a5d4092.js"></script>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                    crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                    crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
                    integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
                    crossorigin="anonymous"></script>
            <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Work+Sans" rel="stylesheet">
            <link rel="stylesheet" href="animate.css">
            <link rel="stylesheet" href="style.css">

        </head>
        <body>

            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                        <img src="image/nouveaux%20s.jpg" alt="photo-un-s" class="photo-nav">
                    </div>
                    <div class="col-xl-1 offset-xl-10 col-lg-1 offset-lg-10 col-md-1 offset-md-10 col-sm-1 offset-sm-10 col-1 offset-10">
                        <div class="dropdown">
                            <button  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="homeDropdown">
                                <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="home.php">home</a>
                                <a class="dropdown-item" href="contemporain.php">contemporary</a>
                                <a class="dropdown-item" href="modern.php">modern</a>
                                <a class="dropdown-item" href="histoire.php">history</a>
                                <a class="dropdown-item" href="objectif.php">objectifs</a>
                                <a class="dropdown-item" href="buy.php">buy</a>
                                <a class="dropdown-item" href="contacts.php">contacts</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-2 offset-xl-1 col-lg-2 offset-lg-1 col-md-2 offset-md-1 align-self-center titleMain">
                        <p class="titre-home">furniture / accessory<br/>
                        <p class="sous-titre-home"><strong>Snow Theme</strong></p>
                    </div>
                    <div class="col-xl-4 offset-xl-1  col-lg-4 offset-lg-1 col-md-4 offset-md-1 col-sm-4 offset-sm-1 col-4 " >
                        <img src="image/photo-ipad.png"  alt="photo-ipad-mode" class="taille-photo-accesoire"  />
                    </div>
                    <div class="col-xl-1 offset-xl-3 col-lg-2  align-self-center">
                        <p class="lettre-home">
                            B<br/>
                            K<br/>
                            S<br/>
                            P
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2 offset-xl-1 col-lg-2 offset-lg-1 col-md-2 offset-md-1 col-sm-3 offset-sm-1 col-3 offset-1">
                        <p class="last-text-home">view project</p>
                    </div>
                    <div class="col-xl-1 offset-xl-8 col-lg-1 offset-lg-8 col-md-1 offset-md-7 col-sm-1 offset-sm-6 col-1 offset-6">
                        <p class="chiffre-home">03</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid social-network-index">
                <div class="row">
                    <div class="col-xl-3 offset-xl-9 col-lg-3 offset-lg-9 col-md-4 offset-md-8 col-sm-4 offset-sm-7 col-6 offset-6">
                        <ul class="social-network social-circle">
                            <li><a href="https://www.facebook.com/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.linkedin.com/" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </body>
    </html>

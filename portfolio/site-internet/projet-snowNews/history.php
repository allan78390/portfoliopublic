<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <script src="https://use.fontawesome.com/720a5d4092.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="style.css">

</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <div class="col-xl-1 col-lg-1 col-md-1">
                <a class="navbar-brand" href="#">Snow</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="col-xl-5 offset-xl-6 col-lg-5 offset-lg-5 col-md-5 offset-md-5">
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="contemporain.php">Contemporary</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="modern.php">Modern</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="histoire.php">History</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="objectif.php">Objectif</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="buy.php">Buy</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="contacts.php">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-3 col-md-8 offset-md-3 col-sm-11 offset-sm-1">
                    <p class="titre-history"><strong>Think. Design. Develop</strong></p>
                    <p class="sous-titre-history">We are a new design based in USA.We have over 10 years of combined experience,<br/>and knows a thing or two aboutdesigning websites and mobile apps.Clever use of technology<br/>and lean processes enable use to work faster and smarter.</p>
                </div>
            </div>
    </div>
    <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/lit1.webp">
                        </div>
                        <div class="back1">
                            <h2>Prix: 2400€</h2>
                            <h3> <strong>Description</strong>: Ce joli lit dont le moelleux vous fera tourner la tête!!</h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/ananas.jpeg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 60€</h2>
                            <h3><strong>Description</strong>: Un dégradé de couleurs exeptionnelles ! </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/lit2.webp">
                        </div>
                        <div class="back1">
                            <h2>Prix: 2700€</h2>
                            <h3> <strong>Description</strong>: Un lit parfait pour passer de douce nuit. </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/table-fleur.jpg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 5400€</h2>
                            <h3> <strong>Description</strong>: Inspiré d'une fleur rare du Népal. </h3>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/bouteille%20de%20vin.jpeg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 700€</h2>
                            <h3> <strong>Description</strong>: Romano CONTIE 1790 </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/taillere.jpg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 11750€</h2>
                            <h3> <strong>Description</strong>: Théière de la dynastie Mang </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image" src="image/chaise%20cuir.jpg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 3200€</h2>
                            <h3> <strong>Description</strong>: Chaise Louis Vuiton sans contrefaçon </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image" src="image/diaments.jpeg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 10000€</h2>
                            <h3> <strong>Description</strong>: Diamant de 35 carat </h3>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image" src="image/cocoon.jpg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 2800€</h2>
                            <h3> <strong>Description</strong>: Vous ne vous assirez plus autre part ! </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                        <div class="info-card1">
                            <div class="front1">
                                <img class="card-image" src="image/table-bleu.jpg">
                            </div>
                            <div class="back1">
                                <h2>Prix: 4200€</h2>
                                <h3> <strong>Description</strong>: Table chic bleu azur </h3>
                            </div>
                        </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                        <div class="info-card1">
                            <div class="front1">
                                <img class="card-image" src="image/luminaire.jpg">
                            </div>
                            <div class="back1">
                                <h2>Prix: 8200€</h2>
                                <h3> <strong>Description</strong>: Luminaire qui illuminera votre vie de mille couleurs </h3>
                            </div>
                        </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
                    <div class="info-card1">
                        <div class="front1">
                            <img class="card-image1" src="image/hermes%20sac.jpg">
                        </div>
                        <div class="back1">
                            <h2>Prix: 1700€</h2>
                            <h3> <strong>Description</strong>: Sac Hermès d'une collection rare </h3>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="container-fluid social-network-index-histoire">
        <div class="row">
            <div class="col-xl-3 offset-xl-9 col-lg-3 offset-lg-9 col-md-4 offset-md-8 col-sm-5 offset-sm-7 col-6 offset-6">
                <ul class="social-network social-circle">
                    <li><a href="https://www.facebook.com/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>


</body>
</html>
